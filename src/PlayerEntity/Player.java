package PlayerEntity;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import MazeGameMain.KeyMovementHandling;
import MazeGameMain.MazeGameWindow;
import MazeGameMain.UtilityTool;

public class Player extends Entity {
	
    KeyMovementHandling keyH;
    
    public final int screenX;
    public final int screenY; //cord where we draw the player
    public int hasKey = 0;
    
    public Player(MazeGameWindow gp, KeyMovementHandling keyH) {
    	
    	super(gp); //passing to entity
    	
    	this.keyH = keyH;
    	
    	screenX = gp.screenWidth/2 - (gp.TileSize/2);
    	screenY = gp.screenHeight/2 - (gp.TileSize/2); //subtracts half tile lenght so its in the center of the tile
    	
    	solidArea = new Rectangle();
    	solidArea.x = 8;
    	solidArea.y = 16;
    	solidAreaDefaultX = solidArea.x;
    	solidAreaDefaultY = solidArea.y;
    	solidArea.width = 32;
    	solidArea.height = 32;
    	
    	setDefaultValues();
    	getPlayerImage();
    }
    public void setDefaultValues() {
    	
    	worldX = gp.TileSize * 38;
    	worldY = gp.TileSize * 43;
        speed = 4;
        direction = "down";
    }
    public void getPlayerImage() {
    	
   	    try {   		  	
    	up1 = ImageIO.read(getClass().getResourceAsStream("/player/hero_up1.png"));
    	up2 = ImageIO.read(getClass().getResourceAsStream("/player/hero_up2.png"));
    	down1 = ImageIO.read(getClass().getResourceAsStream("/player/hero_down1.png"));
    	down2 = ImageIO.read(getClass().getResourceAsStream("/player/hero_down2.png"));
    	left1 = ImageIO.read(getClass().getResourceAsStream("/player/hero_left1.png"));
    	left2 = ImageIO.read(getClass().getResourceAsStream("/player/hero_left2.png"));
    	right1 = ImageIO.read(getClass().getResourceAsStream("/player/hero_right1.png"));
    	right2 = ImageIO.read(getClass().getResourceAsStream("/player/hero_right2.png"));
   	   }catch(IOException e) {
   		e.printStackTrace();
   	   }
    //	up1 = setup("/player/hero_up1");
   //     up2 = setup("/player/hero_up2");
  //      down1 = setup("/player/hero_down1");
  //      down2 = setup("/player/hero_down2");
  //      left1 = setup("/player/hero_left1");
   //     left2 = setup("/player/hero_left2");
   //     right1 = setup("/player/hero_right1");
   //     right2 = setup("/player/hero_right2");
    	
    }
    
   // public BufferedImage setup(String imageName) {
    	
  //  	UtilityTool uTool = new UtilityTool();
   // 	BufferedImage image = null;
    	
   // 	try {
    //		image = ImageIO.read(getClass().getResourceAsStream(imageName + ".png"));
    //		image = uTool.scaleImage(image, gp.TileSize, gp.TileSize);
    //	}catch(IOException e) {
   // 		e.printStackTrace();
   // 	}
    //	return image;
  //  }
    
    public void update() {
    	
    	if(keyH.upPressed == true || keyH.downPressed == true || 
    			keyH.leftPressed == true || keyH.rightPressed == true) {
    		
    		if(keyH.upPressed == true) {
    			direction = "up";
    			
    		}
    		else if(keyH.downPressed == true) {
    			direction = "down";
    			
    		}
    		else if(keyH.leftPressed == true) {
    			direction = "left";
    			
    		}
    		else if(keyH.rightPressed == true) {
    			direction = "right";
    			
    		}
    		
    		//checking collision of tiles
    		collisionOn = false;
    		gp.cDetector.checkTile(this);
    		
    		//check obj coll
    		int objIndex = gp.cDetector.checkObject(this, true);
    		pickUpObject(objIndex);
    		
    		//check collision npc
    		int npcIndex = gp.cDetector.checkEntity(this, gp.npc);
    		interactnpc(npcIndex);
    		
    		//if collision is false,player can go trough
    		if(collisionOn == false) {
    			switch(direction) {
    			case "up":
    				worldY -= speed;
    				break;
    			case "down":
    				worldY += speed;
    				break;
    			case "left":
    				worldX -= speed;
    				break;
    			case "right":
    				worldX += speed;
    				break;
    			}
    		}
    		
    		spriteCounter++;
    		if(spriteCounter > 12) {
    			if(spriteNum == 1) {
    				spriteNum = 2;
    			}
    			else if(spriteNum == 2) {
    				spriteNum = 1;
    			}
    			spriteCounter = 0;
    		}
    	}
    }	
		
    public void pickUpObject(int i) {
    	
    	if(i != 999) { //if its not 999 it means we touched an obj
    		
    		String objectName = gp.obj[i].name;
    		
    		switch(objectName) {
    		case "key":
    			gp.playSE(1);
    			hasKey++;
    			gp.obj[i] = null;
    			gp.ui.showMessage("Picked Up A Key!");
    			break;
    		case "door":
    			if(hasKey > 0) {
        			gp.playSE(3);
    				gp.obj[i] = null;
    				hasKey--;
    				gp.ui.showMessage("You Unlocked The Door!");
    			} else {
    				gp.ui.showMessage("You Need A Key!");
    			}
    			break;
    		case "yeezies":
    			gp.playSE(2);
    			speed += 3;
    			gp.obj[i] = null;
    			gp.ui.showMessage("Picked Up Booster Yeezies");
    			break;
    		case "chest":
    			gp.ui.gameFinished = true;
    			gp.stopMusic();
    			gp.playSE(4);
    			break;
    		}
    		
    	}
    }
    
    public void interactnpc(int i) {
    	if(i != 999) {
    		System.out.println("you are hitting an npc!");
    	}
    }
    
    public void draw(Graphics2D g2) {

    	BufferedImage image = null;
    	
    	switch(direction) {
    	case "up":
    		if(spriteNum == 1) {
    			image = up1;
    		}
    		if(spriteNum == 2) {
    			image = up2;
    		}
    		break;
    		
    	case "down":
    		if(spriteNum == 1) {
    			image = down1;
    		}
    	    if(spriteNum == 2) {
    	    	image = down2;
    	    }
    	    break;
    	
    	case "left":
    		if(spriteNum == 1) {
    			image = left1;
    		}
    	    if(spriteNum == 2) {
    	    	image = left2;
    	    }
    	    break;
    	
    	case "right":
    		if(spriteNum == 1) {
    			image = right1;
    		}
    	    if(spriteNum == 2) {
    	    	image = right2;
    	    }
    	    break;  	
    	}
    	
    	int x = screenX;
    	int y = screenY;
    	
    	if(screenX > worldX) {
    		x = worldX;
    	}
    	if(screenY > worldY) {
    		y = worldY;
    	}
    	int rightOffset = gp.screenWidth - screenX;
		if(rightOffset > gp.worldWidth - worldX) {
			x = gp.screenWidth - (gp.worldWidth - worldX);
		}
		int bottomOffset = gp.screenHeight - screenY;
		if(bottomOffset > gp.worldHeight - worldY) {
			y = gp.screenHeight - (gp.worldHeight - worldY);
		}
    	
    	//g2.drawImage(image, screenX ,screenY, gp.TileSize, gp.TileSize ,null);
    	g2.drawImage(image, x, y, gp.TileSize, gp.TileSize ,null);
    }
}
