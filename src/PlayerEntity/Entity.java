package PlayerEntity;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import MazeGameMain.MazeGameWindow;

public class Entity {
	
    MazeGameWindow gp;
    
	public int worldX, worldY;
	public int speed;
	
	public BufferedImage up1, up2, down1, down2, left1, left2, right1, right2;
	public String direction;
	
	public int spriteCounter = 0;
	public int spriteNum = 1;
	
	public Rectangle solidArea = new Rectangle(0, 0, 48, 48);
	public int solidAreaDefaultX, solidAreaDefaultY;
	public boolean collisionOn = false;
	
	public int actionLockCounter = 0;
	
	public Entity(MazeGameWindow gp) {
		this.gp = gp;
	}
	
	public void setAction() {//simple AI implementation
		
	}
	public void update() {//simple AI implementation
		setAction(); //it takes setAction in wizzard class for a priority
		
		collisionOn = true;
		gp.cDetector.checkTile(this);
		gp.cDetector.checkObject(this, false);
		gp.cDetector.checkPlayer(this);
		
		if(collisionOn == false) {
			switch(direction) {
			case "up":
				worldY -= speed;
				break;
			case "down":
				worldY += speed;
				break;
			case "left":
				worldX -= speed;
				break;
			case "right":
				worldX += speed;
				break;
			}
		}
		
		spriteCounter++;
		if(spriteCounter > 12) {
			if(spriteNum == 1) {
				spriteNum = 2;
			}
			else if(spriteNum == 2) {
				spriteNum = 1;
			}
			spriteCounter = 0;
		}
	}
	
	
	public void draw(Graphics2D g2) {
		
		BufferedImage image = null;
		
		int screenX = worldX - gp.player.worldX + gp.player.screenX;
		int screenY = worldY - gp.player.worldY + gp.player.screenY;
		
		if(gp.player.screenX > gp.player.worldX) {
			screenX = worldX;
		}
		if(gp.player.screenY > gp.player.worldY) {
			screenY = worldY;
		}
		int rightOffset = gp.screenWidth - gp.player.screenX;
		
		if(rightOffset > gp.worldWidth - gp.player.worldX) {
			screenX = gp.screenWidth - (gp.worldWidth - worldX);
		}
		int bottomOffset = gp.screenHeight - gp.player.screenY;
		
		if(bottomOffset > gp.worldHeight - gp.player.worldY) {
			screenY = gp.screenHeight - (gp.worldHeight - worldY);
		}
		
		if(worldX + gp.TileSize > gp.player.worldX - gp.player.screenX &&
		   worldX - gp.TileSize < gp.player.worldX + gp.player.screenX &&
		   worldY + gp.TileSize > gp.player.worldY - gp.player.screenY &&
		   worldY - gp.TileSize < gp.player.worldY + gp.player.screenY) {
			
	    	switch(direction) {
	    	case "up":
	    		if(spriteNum == 1) {
	    			image = up1;
	    		}
	    		if(spriteNum == 2) {
	    			image = up2;
	    		}
	    		break;
	    		
	    	case "down":
	    		if(spriteNum == 1) {
	    			image = down1;
	    		}
	    	    if(spriteNum == 2) {
	    	    	image = down2;
	    	    }
	    	    break;
	    	
	    	case "left":
	    		if(spriteNum == 1) {
	    			image = left1;
	    		}
	    	    if(spriteNum == 2) {
	    	    	image = left2;
	    	    }
	    	    break;
	    	
	    	case "right":
	    		if(spriteNum == 1) {
	    			image = right1;
	    		}
	    	    if(spriteNum == 2) {
	    	    	image = right2;
	    	    }
	    	    break;  	
	    	}
			
			g2.drawImage(image, screenX, screenY, gp.TileSize, gp.TileSize, null);
		}
	}
}
