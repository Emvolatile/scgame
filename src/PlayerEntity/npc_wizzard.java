package PlayerEntity;

import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import MazeGameMain.MazeGameWindow;

public class npc_wizzard extends Entity {
    
	public npc_wizzard(MazeGameWindow gp) {
		super(gp);
		
		direction = "down";
		speed = 1;
		
		getPlayerImage();
	}
    public void getPlayerImage() {
    	
   	    try {   		  	
    	up1 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_backleft.png"));
    	up2 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_backright.png"));
    	down1 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_left.png"));
    	down2 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_left.png"));
    	left1 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_left.png"));
    	left2 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_left.png"));
    	right1 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_right.png"));
    	right2 = ImageIO.read(getClass().getResourceAsStream("/npc/wizzard_right.png"));
   	   }catch(IOException e) {
   		e.printStackTrace();
   	   }
    }
public void setAction() { //simple AI implementation
	
	    actionLockCounter ++;
	    
	    if(actionLockCounter == 180) {
	    	Random random = new Random();
			int i = random.nextInt(100)+1; // picks num from 1-100
			
			if(i <= 25) {
				direction = "up";
			}
			if(i > 25 && i <= 50) {
				direction = "down";
			}
			if(i > 50 && i <= 75) {
				direction = "left";
			}
			if(i > 75 && i <= 100) {
				direction = "right";
			}
	    }
	    actionLockCounter = 0;
	
	}
}
