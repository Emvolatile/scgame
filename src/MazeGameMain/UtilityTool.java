package MazeGameMain;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class UtilityTool {
    
	public BufferedImage scaleImage(BufferedImage original, int width, int height ) {
		BufferedImage scaledImage = new BufferedImage(width,height, original.getType());
		Graphics2D g2 = scaledImage.createGraphics();//wtvr g2 draws is saved in scaledImage
		g2.drawImage(original, 0, 0, width, height, null); //rendering optimisation(loading graphics beforehand)
		g2.dispose();
		
		return scaledImage;
	}
}
