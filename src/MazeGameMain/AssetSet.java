package MazeGameMain;

import PlayerEntity.npc_wizzard;
import object.obj_chest;
import object.obj_door;
import object.obj_key;
import object.obj_yeezies;

public class AssetSet {

	MazeGameWindow gp;
	
	public AssetSet(MazeGameWindow gp) {
		this.gp = gp;
	}
	public void setObject() {
		
		gp.obj[0] = new obj_key(gp);
		gp.obj[0].worldX = 23 * gp.TileSize;
		gp.obj[0].worldY = 7 * gp.TileSize;
		
		gp.obj[1] = new obj_key(gp);
		gp.obj[1].worldX = 23 * gp.TileSize;
		gp.obj[1].worldY = 40 * gp.TileSize;
		
		gp.obj[2] = new obj_key(gp);
		gp.obj[2].worldX = 38 * gp.TileSize;
		gp.obj[2].worldY = 8 * gp.TileSize;
		
		gp.obj[3] = new obj_door();
		gp.obj[3].worldX = 10 * gp.TileSize;
		gp.obj[3].worldY = 11 * gp.TileSize;
		
		gp.obj[4] = new obj_door();
		gp.obj[4].worldX = 8 * gp.TileSize;
		gp.obj[4].worldY = 28 * gp.TileSize;
		
		gp.obj[5] = new obj_door();
		gp.obj[5].worldX = 12 * gp.TileSize;
		gp.obj[5].worldY = 22 * gp.TileSize;
		
		gp.obj[6] = new obj_chest();
		gp.obj[6].worldX = 11 * gp.TileSize;
		gp.obj[6].worldY = 1 * gp.TileSize;
		
		gp.obj[7] = new obj_yeezies();
		gp.obj[7].worldX = 40 * gp.TileSize;
		gp.obj[7].worldY = 2 * gp.TileSize;
	} 
	
	public void setNPC() {
		gp.npc[0] = new npc_wizzard(gp);
		gp.npc[0].worldX = gp.TileSize*34;
		gp.npc[0].worldY = gp.TileSize*37;
	}
	
}
