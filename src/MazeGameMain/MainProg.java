package MazeGameMain;

import javax.swing.JFrame;

public class MainProg {
	public static void main(String[] args) {
		
		JFrame window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.setTitle("Treasure Hunter");
		
		MazeGameWindow MazeGameWindow = new MazeGameWindow();
		window.add(MazeGameWindow);
		
		window.pack();
		
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		
		MazeGameWindow.setGame();
		MazeGameWindow.startGameThread();
	}

}
