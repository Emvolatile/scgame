package MazeGameMain;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import PlayerEntity.Entity;
import PlayerEntity.Player;
import object.SpecialObject;
import tile.TileManager;

public class MazeGameWindow extends JPanel implements Runnable{
    //window set
	final int originalTilesize = 16;
	final int scale = 3;
	public final int TileSize = originalTilesize * scale; //3x razmera
	public final int maxScreenCol = 16;
	public final int maxScreenRow = 12;
	public final int screenWidth = TileSize * maxScreenCol; //786
	public final int screenHeight = TileSize * maxScreenRow; //576
	
	//world settings
	
	public final int maxWorldCol = 50;
	public final int maxWorldRow = 50;
	public final int worldWidth = TileSize * maxWorldCol;
	public final int worldHeight = TileSize * maxWorldRow;
	
	//fps
	int fps = 60;
	
	// system
	TileManager tileM = new TileManager(this);
	KeyMovementHandling keyH = new KeyMovementHandling(this);
	sound music = new sound();
	sound se = new sound();
	public CollisionDetector cDetector = new CollisionDetector(this);
	public AssetSet aSet = new AssetSet(this);
	public UI ui = new UI(this);
	Thread gamethread;
	
	//entity & object
	public Player player = new Player(this, keyH);
	public SpecialObject obj[] = new SpecialObject[10]; // creation of 10 slots for object in the parent class
		//this means up to 10 obj can be displayed at a time
	public Entity npc[] = new Entity[10];
	
	//game state
	public int gameState;
	public final int playState = 1;//implementing state of the game so it works as expected in different sections
	public final int pauseState = 2;
	
	
	public MazeGameWindow() {
		this.setPreferredSize(new Dimension(screenWidth, screenHeight));
		this.setBackground(Color.black);
		this.setDoubleBuffered(true);
		this.addKeyListener(keyH);
		this.setFocusable(true);
	}

	public void setGame() {
		aSet.setObject();
		aSet.setNPC();
		playMusic(0);
		gameState = playState;
	}
	
	public void startGameThread() {
		gamethread = new Thread(this);
		gamethread.start();
	}
	
    public void run() {
    	
    	double drawInterval = 1000000000/fps;
    	double loopster = 0;
    	long lastTime = System.nanoTime();
    	long currentTime;
    	long timer = 0;
    	int drawCount = 0;
    	
    	while(gamethread != null) {
    		
    		currentTime = System.nanoTime();
    		
    		loopster += (currentTime - lastTime) / drawInterval;
    		timer +=(currentTime - lastTime);
    		
    		lastTime = currentTime;
    		
    		if(loopster >= 1) {
    			
    		
    		update();
    		repaint();
    		loopster--;
    		drawCount++;
    		}
    		if(timer >= 1000000000) {
    			// System.out.println("FPS:" + drawCount);
    			drawCount = 0;
    			timer = 0;
    		}
    	}
    }
    
	public void update() { //pausing game state creation
		  if(gameState == playState) {
			  // player
			  player.update();
			  //npc
			  for(int i = 0; i < npc.length; i++) {
				  if(npc[i] != null) {
					  npc[i].update();
				  }
			  }
		  }
		  
          if(gameState == pauseState) {
        	  //nothing happens
          }
          
	}
	public void paintComponent(Graphics g) {
		//painbrsuhlike section 
		super.paintComponent(g);
		
		Graphics2D g2 =  (Graphics2D)g;
		
		//debugger time in nano
		
		long drawStart = 0;
		if(keyH.checkDrawTime == true) {
			drawStart = System.nanoTime();
		}
		
		//tile
		tileM.draw(g2);
		//input tile bfr player(layer management)
		//object
		for(int i = 0; i < obj.length; i++) { //scanning arrays of object and super object
			if(obj[i] != null) {
				obj[i].draw(g2, this); 
				
				//scanning every slot to see if theres anything in em
				
			}
		}
		
		//npc
		for(int i = 0; i < npc.length; i++) {
			if(npc[i] != null) {
				npc[i].draw(g2);
			}
		}
		
		//player
		player.draw(g2);
		
		//UI
		ui.draw(g2);
		
		//debug too
		if(keyH.checkDrawTime == true) {
			long drawEnd = System.nanoTime();
			long passed = drawEnd - drawStart;
			g2.setColor(Color.WHITE);
			g2.drawString("Draw Time:" + passed, 10, 400);
			System.out.println("Draw Time:" + passed);
		}		
		
		g2.dispose();
	}
	
	public void playMusic(int i) {
		
		music.setFile(i);
		music.play();
		music.loop();
	}
	public void stopMusic() {
		music.stop();
	}
	public void playSE(int i) {
		se.setFile(i);
		se.play();
	}
	
}
