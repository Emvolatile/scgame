package MazeGameMain;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;

import object.obj_key;

public class UI {
    
	MazeGameWindow gp;
	Graphics2D g2;
	Font arial_40, arial_80B;
	BufferedImage keyImage;
	public boolean messageON = false;
	public String message = "";
	int messageCounter = 0;
	public boolean gameFinished = false;
	
	double playTime;
	DecimalFormat dFormat = new DecimalFormat("#0.00");
	
	
	public UI(MazeGameWindow gp) {
		this.gp = gp;
		
		arial_40 = new Font("Arial", Font.PLAIN, 40);
		arial_80B = new Font("Arial", Font.BOLD, 80);
		obj_key key = new obj_key(gp);
		keyImage = key.image;
	}
	public void showMessage(String text) {
		message = text;
		messageON = true;
	}
	public void draw(Graphics2D g2) {
		
		if(gameFinished == true) {
			
			g2.setFont(arial_40);
			g2.setColor(Color.white);
			
			String text;
			int x;
			int y;
			int textLength;
			
			text = "You Found The Treasure Chest!";
			textLength = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth(); //returns length of the text
			
			
			x = gp.screenWidth/2 - textLength/2;
			y = gp.screenHeight/2 - (gp.TileSize*3);
			g2.drawString(text, x, y);
			
			g2.setFont(arial_80B);
			g2.setColor(Color.yellow);
			
			text = "You Won!";
			textLength = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth(); //returns length of the text
			x = gp.screenWidth/2 - textLength/2;
			y = gp.screenHeight/2 + (gp.TileSize*2);
			g2.drawString(text, x, y);
			
			g2.setFont(arial_80B);
			g2.setColor(Color.WHITE);
			
			text = "Your Time:" + dFormat.format(playTime);
			textLength = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth(); //returns length of the text		
			x = gp.screenWidth/2 - textLength/2;
			y = gp.screenHeight/2 + (gp.TileSize*4);
			g2.drawString(text, x, y);
			
			gp.gamethread = null;
		} else {
			g2.setFont(arial_40);
			g2.setColor(Color.white);
			g2.drawImage(keyImage, gp.TileSize/2, gp.TileSize/2, gp.TileSize, gp.TileSize, null);
			g2.drawString("x " + gp.player.hasKey, 74, 65);
			
			//time 
			playTime += (double)1/60;
			g2.drawString("Time:" + dFormat.format(playTime), gp.TileSize*11, 65);
			
			//message
			
			if(messageON == true) {
				
				g2.setFont(g2.getFont().deriveFont(30F));
				g2.drawString(message, gp.TileSize/2, gp.TileSize*5);
				
				messageCounter++;
				
				if(messageCounter > 120) {
					messageCounter = 0;
					messageON = false;
				}
		}
		
		
		}
		
	 this.g2 = g2;
	 g2.setFont(arial_40);
	 g2.setColor(Color.WHITE);
	 
	 if(gp.gameState == gp.playState) {
		 //playState related
		 
	 } 
	 if(gp.gameState == gp.pauseState) {
		 drawPauseScreen();
	 }
	}
	public void drawPauseScreen() {
		String text = "PAUSED";
		int x = getXCenteredText(text);
		int y = gp.screenHeight/2;
		
		g2.drawString(text, x, y);
	}
	public int getXCenteredText (String text) {
		int length = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth();
        int x = gp.screenWidth/2 - length/2;
        return x;
	}
}
