package tile;

import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import MazeGameMain.MazeGameWindow;
import MazeGameMain.UtilityTool;

public class TileManager {
     
	MazeGameWindow gp;
	public tile[] tile;
	public int mapTileNum[] [];
	
	
	public TileManager(MazeGameWindow gp) {
		
		this.gp = gp;
		
		tile = new tile[50];
		mapTileNum = new int [gp.maxWorldCol][gp.maxWorldRow]; //inputting numbers from txt file to map
		
		getTileImage();
		loadMap("/maps/map2.txt"); //loading the map here so I dont have to create new methods every time we want to load a new one
		
	}
	
	public void getTileImage() {
			
		setup(10, "/tiles/grass_short", false);
		setup(11, "/tiles/grass_long", false);
		setup(12, "/tiles/water", true);
		setup(13, "/tiles/water2", true);
		setup(14, "/tiles/water_ul", true);
		setup(15, "/tiles/water_uc", true);
		setup(16, "/tiles/water", true);
		setup(17, "/tiles/water_ml", true);
		setup(18, "/tiles/water_mr", true);
		setup(19, "/tiles/corner_dl", true);
		setup(20, "/tiles/water_dc", true);
		setup(21, "/tiles/water2", true);
		setup(22, "/tiles/water_ur", true);
		setup(23, "/tiles/corner_ur", true);
		setup(24, "/tiles/water", true);
		setup(25, "/tiles/corner_dr", true);
		setup(26, "/tiles/corner_ul", false);
		setup(27, "/tiles/water_dr", false);
		setup(28, "/tiles/water_dl", false);
		setup(29, "/tiles/path", false);
		setup(30, "/tiles/path", false);
		setup(31, "/tiles/path", false);
		setup(32, "/tiles/path", false);
		setup(33, "/tiles/path", false);
		setup(34, "/tiles/path", false);
		setup(35, "/tiles/path", false);
		setup(36, "/tiles/path", false);
		setup(37, "/tiles/path", false);
		setup(38, "/tiles/path", false);
		setup(39, "/tiles/path", false);
		setup(40, "/tiles/bricks", true);
		setup(41, "/tiles/tree1", true);
		
	
			
		
	}
	public void setup(int index, String imageName, boolean collision) {
		UtilityTool uTool = new UtilityTool();
		
		try {
			tile[index] = new tile();
			tile[index].image = ImageIO.read(getClass().getResourceAsStream(imageName + ".png"));
			tile[index].image = uTool.scaleImage(tile[index].image, gp.TileSize, gp.TileSize);	
			tile[index].collision = collision;
		}catch(IOException e) {
			e.printStackTrace(); //collision setting plus tile setup
		}
	}
	public void loadMap(String FilePath) {
		//loading the map by scanning with while loops
		try {
			InputStream is = getClass().getResourceAsStream(FilePath);
			BufferedReader br = new BufferedReader(new InputStreamReader(is)); //buffer reads content of txt file
			
			int col = 0;
			int row = 0;
			
			while(col < gp.maxWorldCol && row < gp.maxWorldRow) { //max limits the data to run in these parameters
				
				String line = br.readLine(); //reads lines of map input one by one
				
				while(col < gp.maxWorldCol) {
					String numbers[] = line.split(" "); //splitting the string by one place imitates the space in the text file
					
					int num = Integer.parseInt(numbers[col]); //splites the lines
					
					mapTileNum[col][row] = num;
					col++;										
				}
				if(col == gp.maxWorldCol) {
					col = 0;
					row++;
				}
			}
			br.close();
		}catch(Exception e) {
			
		}
		
		
		
		
	}
	public void draw(Graphics2D g2) {
		
		int worldCol = 0;
		int worldRow = 0;

		
		while(worldCol < gp.maxWorldCol && worldRow < gp.maxWorldRow) {
			
		    int tileNum = mapTileNum[worldCol][worldRow]; //data is stored here
			//extracts numbers from map file and converts to tile
			
			int worldX = worldCol * gp.TileSize;
			int worldY = worldRow * gp.TileSize;
			int screenX = worldX - gp.player.worldX + gp.player.screenX;
			int screenY = worldY - gp.player.worldY + gp.player.screenY;//implements camera feature (where player needs to be drawn in order for map to move accordingly)
			
			//stop camera going out of bounds
			if(gp.player.screenX > gp.player.worldX) {
				screenX = worldX;
			}
			if(gp.player.screenY > gp.player.worldY) {
				screenY = worldY;
			}
			int rightOffset = gp.screenWidth - gp.player.screenX;
		
			if(rightOffset > gp.worldWidth - gp.player.worldX) {
				screenX = gp.screenWidth - (gp.worldWidth - worldX);
			}
			int bottomOffset = gp.screenHeight - gp.player.screenY;
		
			if(bottomOffset > gp.worldHeight - gp.player.worldY) {
				screenY = gp.screenHeight - (gp.worldHeight - worldY);
			}
			
			if(worldX + gp.TileSize > gp.player.worldX - gp.player.screenX &&
			   worldX - gp.TileSize < gp.player.worldX + gp.player.screenX &&
			   worldY + gp.TileSize > gp.player.worldY - gp.player.screenY &&
			   worldY - gp.TileSize < gp.player.worldY + gp.player.screenY) {
				
				
				g2.drawImage(tile[tileNum].image, screenX, screenY, null); //
			}
			else if(gp.player.screenX > gp.player.worldX ||
					gp.player.screenY > gp.player.worldY ||
					rightOffset > gp.worldWidth - gp.player.worldX ||
					bottomOffset > gp.worldHeight - gp.player.worldY) {
				g2.drawImage(tile[tileNum].image, screenX, screenY, null); //improving camera movement on the edge with cond
			}
			
			worldCol++;
			
			if(worldCol == gp.maxWorldCol) {
				worldCol = 0;
				worldRow++;

			}
		}
	}
}

