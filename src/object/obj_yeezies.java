package object;

import java.io.IOException;

import javax.imageio.ImageIO;

public class obj_yeezies extends SpecialObject {
    
    public obj_yeezies() {
    	
    	name = "yeezies";
    	try {
    		image = ImageIO.read(getClass().getResourceAsStream("/objects/yeezy.png"));
    		
    	} catch(IOException e) {
    		e.printStackTrace();
    	}
    }
}
