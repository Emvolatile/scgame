package object;

import java.awt.image.BufferedImage;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import MazeGameMain.MazeGameWindow;
import MazeGameMain.UtilityTool;

public class SpecialObject {

	public BufferedImage image;
	public String name;
	public boolean collision = false;
	public int worldX, worldY;
	public Rectangle solidArea = new Rectangle(0, 0, 48, 48); //specific solid field
	public int solidAreaDefaultX = 0;
	public int solidAreaDefaultY = 0;
	UtilityTool uTool = new UtilityTool();
	
	public void draw(Graphics2D g2, MazeGameWindow gp) {
		int screenX = worldX - gp.player.worldX + gp.player.screenX;
		int screenY = worldY - gp.player.worldY + gp.player.screenY;
		
		if(gp.player.screenX > gp.player.worldX) {
			screenX = worldX;
		}
		if(gp.player.screenY > gp.player.worldY) {
			screenY = worldY;
		}
		int rightOffset = gp.screenWidth - gp.player.screenX;
		
		if(rightOffset > gp.worldWidth - gp.player.worldX) {
			screenX = gp.screenWidth - (gp.worldWidth - worldX);
		}
		int bottomOffset = gp.screenHeight - gp.player.screenY;
		
		if(bottomOffset > gp.worldHeight - gp.player.worldY) {
			screenY = gp.screenHeight - (gp.worldHeight - worldY);
		}
		
		if(worldX + gp.TileSize > gp.player.worldX - gp.player.screenX &&
		   worldX - gp.TileSize < gp.player.worldX + gp.player.screenX &&
		   worldY + gp.TileSize > gp.player.worldY - gp.player.screenY &&
		   worldY - gp.TileSize < gp.player.worldY + gp.player.screenY) {
			
			
			
			g2.drawImage(image, screenX, screenY, gp.TileSize, gp.TileSize, null);
		}
		else if(gp.player.screenX > gp.player.worldX ||
				gp.player.screenY > gp.player.worldY ||
				rightOffset > gp.worldWidth - gp.player.worldX ||
				bottomOffset > gp.worldHeight - gp.player.worldY) {
			g2.drawImage(image, screenX, screenY, gp.TileSize, gp.TileSize, null); //improving camera movement on the edge with cond
		}
	}
	
}
