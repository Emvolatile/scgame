package object;

import java.io.IOException;

import javax.imageio.ImageIO;

import MazeGameMain.MazeGameWindow;

public class obj_key extends SpecialObject{
	
	MazeGameWindow gp;
	
    public obj_key(MazeGameWindow gp) {
    	
    	this.gp = gp;
    	
    	name = "key";
    	try {
    		image = ImageIO.read(getClass().getResourceAsStream("/objects/key1.png"));
    		uTool.scaleImage(image, gp.TileSize, gp.TileSize);
    		
    		
    	} catch(IOException e) {
    		e.printStackTrace();
    	}
    }
}
